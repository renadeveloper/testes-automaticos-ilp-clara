#check if the answersheet  is valid
def check_answersheet(answersheet, len_answersheet):
    if len(answersheet) != len_answersheet:
        return False
    else:
        for letter in answersheet:
            if letter not in ['a', 'b', 'c', 'd', 'e']:
                return False
        return True

#checks if the standard number already exists
def exists_student_by_std_number(std_number, students_info):
    for i in range(len(students_info)):
        if std_number == students_info[i][1]:
            return True
    return False


#register students
def register_students(students_infos, n_students, len_answersheet,
                      answersheet):
    for student in range(n_students):
        name = input('Digite o nome do(a) aluno(a): ')
        std_number = input('Digite a matricula do(a) aluno(a): ')
        answer = input('Digite o gabarito do(a) aluno(a): ').split()
        if exists_student_by_std_number(std_number, students_infos):
            print('Estudante com esta matrícula já existe!')
            exit()
        if check_answersheet(answer, len_answersheet) == True:
            grade = grading_sys(answer, answersheet)
            infos = [name, std_number, answer, grade]
            students_infos.append(infos)
        else:
            print('Gabarito invalido')
            exit()


#grading system
def grading_sys(answer, answersheet):
    grade = 0
    for question in range(len(answersheet)):
        if answer[question] == answersheet[question]:
            grade = grade + 1
    return grade


#calculate the avg grade
def avg(list_grades, n_students):
    avgr = sum(list_grades) / n_students
    return round(avgr,2)
    #print('A nota média da turma é: %.2f ' % (avgr))
    #print()#space between the answer and the menu



#order the students by name
def alpha_order(students_infos):
    students_infos.sort()
    #print('O(s) aluno(s): \n')
    return format_students(students_infos)
    #print()#space between the answer and the menu

#order grades from 0 to 10
def all_grades_order(students_infos):
    students_infos.sort(key=lambda x: x[3])
    #print('Notas em ordem crescente: \n')
    return format_students(students_infos)
    #print()#space between the answer and the menu

#students that grade is greater than 7
def approved_students(students_infos, len_answersheet):
    for student in range(len(students_infos)):
        if students_infos[student][3] >= (len_answersheet * 0.7):
            print('O(s) aluno(s) aprovado(s): \n')
            format_students(students_infos[student], sep='\n')
            print()#space between the answer and the menu

#reproved students
def reproved_students(students_infos, len_answersheet):
    for student in range(len(students_infos)):
        if students_infos[student][3] < (len_answersheet * 0.7):
            print('O(s) aluno(s) reprovado(s): \n')
            format_students(students_infos[student])
            print()#space between the answer and the menu

#percentual of aproved students
def percentual_approved(students_infos, len_answersheet):
    counter = 0
    for student in range(len(students_infos)):
        if students_infos[student][3] >= (len_answersheet * 0.7):
            counter += 1
    resultado_para_teste = round((counter / len(students_infos) * 100),2)
    return resultado_para_teste
    #print('Porcentagem de alunos aprovados: {0:.2f} %'.format(
    #    (counter / len(students_infos) * 100)))

#absolute frequency
def abs_freq(students_infos):
    dict = {}
    for student in range(len(students_infos)):
        grade = students_infos[student][3]
        if grade not in dict.keys():
            dict[grade] = 1
        else:
            dict[grade] += 1
    
    return dict[grade]
    #print('A nota com maior frequência absoluta é:', dict[grade])
    #print()#space between the answer and the menu



#shows the highest grade
def highest_grade_students(students_infos):
    students = []
    highest = highest_grade(students_infos)
    for student in students_infos:
        if student[3] == highest:
            students.append(student)
    #print('O(s) aluno(s) com a nota mais alta: \n')
    return format_students(students)
    #print()#space between the answer and the menu


def lowest_grade_students(students_infos, max_grade):
    students = []
    lowest = lowest_grade(students_infos, max_grade)
    for student in students_infos:
        if student[3] == lowest:
            students.append(student)
    #print('O(s) aluno(s) com a nota mais baixa: \n')
    return format_students(students)
    #print()#space between the answer and the menu


def highest_grade(students_infos):
    highest = 0
    for student in students_infos:
        if student[3] > highest:
            highest = student[3]
    return highest


def lowest_grade(students_infos, max_grade):
    lowest = max_grade
    for student in students_infos:
        if student[3] < lowest:
            lowest = student[3]
    return lowest

def format_students(students):
    lista_info_total_alunos = []
    for student in students:
        lista_info_total_alunos.append(show_user(student))
    return lista_info_total_alunos

def show_user(student):
    lista_info_aluno = [student[0], student[1], student[2], student[3]]
    return lista_info_aluno
    #print('Matricula:',
    #      student[0],
    #      'Nome:',
    #      format_fixed_size(student[1],15),
    #      'Resposta:',
    #      '-'.join(student[2]),
    #      'Nota:',
    #      student[3],
    #      sep='\t')

def format_fixed_size(data, limit, overflow_chars = "..."):
    info = (data[:limit] + (data[limit:] and overflow_chars)) + (max(0, (limit + len(overflow_chars)) - len(data)) * ' ')
    return info