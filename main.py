import menu
import logic
import csv

info_students = []
students_infos = []
list_students = []


initial_question=input('Digite o nome do arquivo com o gabarito para ler ou deixe em branco para continuar: ')
if initial_question != '':
    reader = csv.reader(open(initial_question, "r"), delimiter=';', quotechar="'")
    for line in reader:
        answersheet= line
        len_answersheet = len(line)
        
else:    
    len_answersheet=int(input('Digite o tamanho do gabarito: '))
    answersheet= input('Digite gabarito: ').split()
    logic.check_answersheet(answersheet, len_answersheet)
   
second_question=input('Digite o nome do arquivo de respostas ou então deixe em branco para continuar: ')
if second_question != '':
    reader = csv.reader(open(second_question, "r"), delimiter=';', quotechar="'")
    for line in reader:
        answer=line[2].split(';')
        grade = logic.grading_sys(answer, answersheet)
        students_infos.append([line[0], line[1],answer, grade])
    n_students=len(students_infos)  
else:
    n_students=int(input('Numero de alunos: '))
    logic.register_students(students_infos, n_students, len_answersheet,answersheet)

if n_students<= 0 or len_answersheet<=0:
  print('valores inválidos')
  exit()

while True :
    menu.menu_principal()
    option = int(input('Opção: '))
    if option == 1:
        logic.alpha_order(students_infos)
    elif option == 2:
        logic.all_grades_order(students_infos)
    elif option == 3:
        logic.approved_students(students_infos, len_answersheet)
    elif option == 4:
        logic.reproved_students(students_infos, len_answersheet)
    elif option == 5:
        logic.percentual_approved(students_infos, len_answersheet)
    elif option == 6:
        logic.abs_freq(students_infos)
    elif option == 7:
        logic.highest_grade_students(students_infos)
    elif option == 8:
        logic.lowest_grade_students(students_infos, len_answersheet)
    elif option == 9:
        list_grades = []
        for student in students_infos:
          list_grades.append(student[3])
        logic.avg(list_grades, n_students)
    elif option == 10:
        break
    else:
        print('Opção invalida')

