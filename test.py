import pytest
import logic
import csv


###################### Funções Iniciais de Leitura do Gabarito e Resposta #################################

info_students = []
students_infos = []
list_students = []

initial_question='gabarito.csv'
if initial_question != '':
    reader = csv.reader(open(initial_question, "r"), delimiter=';', quotechar="'")
    for line in reader:
        answersheet= line
        len_answersheet = len(line)
        
else:    
    len_answersheet=int(input('Digite o tamanho do gabarito: '))
    answersheet= input('Digite gabarito: ').split()
    logic.check_answersheet(answersheet, len_answersheet)
   
second_question='respostas.csv'
if second_question != '':
    reader = csv.reader(open(second_question, "r"), delimiter=';', quotechar="'")
    for line in reader:
        answer=line[2].split(';')
        grade = logic.grading_sys(answer, answersheet)
        students_infos.append([line[0], line[1],answer, grade])
    n_students=len(students_infos)  
else:
    n_students=int(input('Numero de alunos: '))
    logic.register_students(students_infos, n_students, len_answersheet,answersheet)

if n_students<= 0 or len_answersheet<=0:
  print('valores inválidos')
  exit()


###################### 1 - A lista em ordem alfabética dos alunos com o número de matricula e a sua nota ###########################

def test_ordem_alfabetica_nome():
    assert logic.alpha_order(students_infos) == [['014', 'Ana Catarina', ['a','b','a','e','a','a','a','b','c','b'], 7],
                                                ['313', 'Bia', ['c','a','d','b','a','b','a','d','c','a'], 3],
                                                ['083','Clara', ['c','e','c','e','b','a','a','b','c','b'], 6],
                                                ['007','Eduardo', ['d','a','d','e','c','a','b','b','c','a'], 5],
                                                ['727','Felipe', ['d','e','a','d','c','b','e','d','a','b'], 0],
                                                ['666', 'Fernanda', ['a','d','d','e','c','d','e','a','a','a'], 3],
                                                ['707', 'Jorel', ['a','e','d','e','d','a','a','b','c','b'], 7]],"test sucessful"
    

###################### 2- A lista em ordem crescente de notas com o nome do aluno, o numero da matricula e a sua nota. #############
      
def test_ordem_crescente_nota():
    assert logic.all_grades_order(students_infos) == [['727','Felipe', ['d','e','a','d','c','b','e','d','a','b'], 0],
                                                    ['313', 'Bia', ['c','a','d','b','a','b','a','d','c','a'], 3],
                                                    ['666', 'Fernanda', ['a','d','d','e','c','d','e','a','a','a'], 3],
                                                    ['007','Eduardo', ['d','a','d','e','c','a','b','b','c','a'], 5],
                                                    ['083','Clara', ['c','e','c','e','b','a','a','b','c','b'], 6],
                                                    ['014', 'Ana Catarina', ['a','b','a','e','a','a','a','b','c','b'],7],
                                                    ['707', 'Jorel', ['a','e','d','e','d','a','a','b','c','b'], 7]],"test sucessful"


###################### 3. A lista em ordem crescente de notas com o nome do aluno, o numero da matricula #########################
###################### e a sua nota para os alunos aprovados (>= 7.0). ###########################################################

     
def test_ordem_crescente_nota_aprovados():
    assert logic.all_grades_order(students_infos) == [['014', 'Ana Catarina', ['a','b','a','e','a','a','a','b','c','b'],7],
                                                    ['707', 'Jorel', ['a','e','d','e','d','a','a','b','c','b'], 7]],"test sucessful"


###################### 4. A lista em ordem decrescente de notas com o nome do aluno, o numero da #################################
###################### matricula e a sua nota para os alunos reprovados (< 7.0). #################################################


def test_ordem_decrescente_nota_reprovados():
    assert logic.all_grades_order(students_infos) == [['083','Clara', ['c','e','c','e','b','a','a','b','c','b'], 6],
                                                    ['007','Eduardo', ['d','a','d','e','c','a','b','b','c','a'], 5],
                                                    ['313', 'Bia', ['c','a','d','b','a','b','a','d','c','a'], 3],
                                                    ['666', 'Fernanda', ['a','d','d','e','c','d','e','a','a','a'], 3],
                                                    ['727','Felipe', ['d','e','a','d','c','b','e','d','a','b'], 0]],"test sucessful"


###################### 5. O percentual de aprovação, sabendo-se que a nota mínima para aprovação é >= 7.0. ######################

def test_percentual_aprovacao():
    assert logic.percentual_approved(students_infos, len_answersheet) == 28.57


###################### 6. A nota que teve a maior frequência absoluta. ##########################################################

logic.abs_freq(students_infos)
def test_frequencia_absoluta():
    assert logic.abs_freq(students_infos) == 3 or logic.abs_freq(students_infos) == 7


###################### 7. O aluno com a maior nota (nome, matricula e nota). ####################################################


def test_aluno_maior_nota():
    assert logic.highest_grade_students(students_infos) == [['014', 'Ana Catarina', ['a','b','a','e','a','a','a','b','c','b'],7],
                                                            ['707', 'Jorel', ['a','e','d','e','d','a','a','b','c','b'], 7]],"test sucessful"

###################### 8. O aluno com a menor nota (nome, matricula e nota). ####################################################


def test_aluno_menor_nota():
    assert logic.lowest_grade_students(students_infos, len_answersheet) == [['727','Felipe', ['d','e','a','d','c','b','e','d','a','b'], 0]],"test sucessful"


###################### 9. A media da turma. #####################################################################################


def test_media_turma():
    list_grades = []
    for student in students_infos:
        list_grades.append(student[3])
    assert logic.avg(list_grades, n_students) == 4.43,"test sucessful"
